package java6;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class Uzduotis6 {
    public static void main(String[] args) {
        String failoKelias = new File("").getAbsolutePath()
                + "/src/java6/Duomenys.txt";
        String[] vardai = new String[3];
        String[] pavardes = new String[3];
        Integer[] amziai = new Integer[3];
        String[] pareigybes = new String[3];
        skaitymas(failoKelias, vardai, pavardes, amziai, pareigybes);
        spausdinti(vardai, pavardes, amziai, pareigybes);

    }

    public static void skaitymas(String failoKelias, String[] vardai,
                                 String[] pavardes, Integer[] amziai,
                                 String[] pareigybes) {
        try (BufferedReader skaitytuvas = new BufferedReader(new FileReader(failoKelias))) {
            String eilute = skaitytuvas.readLine();
            Integer indeksas = 0;
            while (eilute != null) {
                String[] eilDuomenys = eilute.split(" ");
                vardai[indeksas] = eilDuomenys[0];
                pavardes[indeksas] = eilDuomenys[1];
                amziai[indeksas] = Integer.parseInt(eilDuomenys[2]);
                pareigybes[indeksas] = eilDuomenys[3];
                indeksas++;
                eilute = skaitytuvas.readLine();
            }

        } catch (FileNotFoundException ex) {
            System.out.println("Failas nerastas");
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    static void spausdinti(String[] vardai,
                           String[] pavardes, Integer[] amziai,
                           String[] pareigybes) {
        for(int i = 0; i < vardai.length; i++) {
            System.out.println(vardai[i]  + "-" + pavardes[i] + "-"+ amziai[i] + "-" + pareigybes[i]);
        }
    }
}
