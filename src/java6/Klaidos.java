package java6;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class Klaidos {
    public static void main(String[] args) {
//        Integer[] masyvas = new Integer[3];
//        masyvas[2] = 8;
////        throw new ArrayIndexOutOfBoundsException("ismete klaida");
//        try {
//            System.out.println(masyvas[9]);
//        } catch (ArrayIndexOutOfBoundsException ex) {
//            System.out.println(masyvas[masyvas.length - 1]);
//        } catch (NullPointerException ex) {
//            System.out.println("Veiksmai is null");
//        } finally {
//            System.out.println("finally blokas");
//        }
//        System.out.println("Kodas zemiau");

//        String failoKelias = new File("").getAbsolutePath()
//                + "/src/java6/Duomenys.txt";
//
//        skaitymas("", failoKelias);

        Integer[] masyvas = {1, 2, 3, 4, 5};
        Integer reiksme = grazintiReiksme(masyvas, 99);
        System.out.println(reiksme);

        Integer paverstas = pakeistiTipa("5a5");
        System.out.println(paverstas);
    }

    public static Integer grazintiReiksme(Integer[] masyvas, Integer indeksas) {
        try {
            return masyvas[indeksas];
        } catch (ArrayIndexOutOfBoundsException ex) {
            System.out.println("Ivestas neegzistuojantis indeksas");
            return masyvas[masyvas.length - 1];
        }
    }

    public static Integer pakeistiTipa(String skaicius) {
        try {
            return Integer.parseInt(skaicius);
        } catch (NumberFormatException ex) {
            System.out.println("Nepavyko paversti i skaicius");
            return null;
        }
    }

//    public static Character gautiRaide(String zodis, Integer indeksas) {
////
////    }


    public static void skaitymas(String failoKelias, String visadaEgzistuojantisFailas) {
        try (BufferedReader skaitytuvas = new BufferedReader(new FileReader(failoKelias))) {
            String eilute = skaitytuvas.readLine();
            while (eilute != null) {
                System.out.println(eilute);
                eilute = skaitytuvas.readLine();
            }

        } catch (FileNotFoundException ex) {
            System.out.println("Failas nerastas");
            System.out.println("Naudojamas kitas egzistuojantis failas");
            skaitymas(visadaEgzistuojantisFailas, visadaEgzistuojantisFailas);
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
