package java12;

import java.util.Arrays;

public class BendrineKlase<T> {
    private T objektas;
// set get


    public T get() {
        return objektas;
    }
    public void set(T objektas) {
        this.objektas = objektas;
    }
    public static void main(String args[]) {
        BendrineKlase<Integer> sa = new BendrineKlase<>();
        sa.set(1);
        Integer str = sa.get(); // kastingo nebereikia  !!
        System.out.println(str);

//        BendrineKlase<Double> sa1 = new BendrineKlase<>();
//        sa1.setVardas("Jonas");
//        sa1.set(123d);
//        System.out.println(sa1.getVardas());
    }

}
