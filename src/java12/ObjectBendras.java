package java12;

public class ObjectBendras {
    private Object objektas;

    public Object get() {
        return objektas;
    }
    public void set(Object objektas) {
        this.objektas = objektas;
    }
    public static void main(String args[]) {
        ObjectBendras sa = new ObjectBendras();
        sa.set(1235.2);
        Double str = (Double)sa.get(); // reikia kastingo  !!
        sa.set("Labas");
        System.out.println(sa.get());
    }
}
