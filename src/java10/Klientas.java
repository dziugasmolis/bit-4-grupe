package java10;

import java.util.Arrays;

public class Klientas implements Mokejimas {

    private String bankoSaskaita;
    private String asmensVardas;
    private Double suma;
    private Double[] sumuMasyvas;

    public Klientas(String bankoSaskaita,
                    String asmensVardas,
                    Double suma) {
        this.bankoSaskaita = bankoSaskaita;
        this.asmensVardas = asmensVardas;
        this.suma = suma;
    }

    public Klientas(String bankoSaskaita,
                    String asmensVardas,
                    Double suma,
                    Double[] sumuMasyvas) {
        this(bankoSaskaita, asmensVardas, sumaSuma(sumuMasyvas, suma));
        this.sumuMasyvas = sumuMasyvas;
    }

    public static Double sumaSuma(Double[] sumuMasyvas, Double suma) {
        for (int i = 0; i < sumuMasyvas.length; i++) {
            suma += sumuMasyvas[i];
        }
        return suma;
    }

    public String toString() {
        return "Banko saskaita: " + gautiBankoSaskaita() +
                " Asmens vardas: " + gautiSaskaitosAsmensVarda() +
                " Suma: " + gautiSuma() +
                " Sumos: " + Arrays.toString(this.sumuMasyvas)
                + "\n";
    }

    @Override
    public String gautiBankoSaskaita() {
        return bankoSaskaita;
    }

    @Override
    public String gautiSaskaitosAsmensVarda() {
        return asmensVardas;
    }

    @Override
    public Double gautiSuma() {
        return suma;
    }

    public String getBankoSaskaita() {
        return bankoSaskaita;
    }

    public void setBankoSaskaita(String bankoSaskaita) {
        this.bankoSaskaita = bankoSaskaita;
    }

    public String getAsmensVardas() {
        return asmensVardas;
    }

    public void setAsmensVardas(String asmensVardas) {
        this.asmensVardas = asmensVardas;
    }

    public Double getSuma() {
        return suma;
    }

    public void setSuma(Double suma) {
        this.suma = suma;
    }

    public Double[] getSumuMasyvas() {
        return sumuMasyvas;
    }

    public void setSumuMasyvas(Double[] sumuMasyvas) {
        this.sumuMasyvas = sumuMasyvas;
    }
}
