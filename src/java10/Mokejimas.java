package java10;

public interface Mokejimas {

    String gautiBankoSaskaita();
    String gautiSaskaitosAsmensVarda();
    Double gautiSuma();

}
