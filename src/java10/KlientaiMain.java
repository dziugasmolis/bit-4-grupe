package java10;

import java.util.Arrays;

public class KlientaiMain {
    public static void main(String[] args) {
        Klientas obj1 = new Klientas("LT123",
                "Petras", 50.0, new Double[]{50.0, 99.9, 100.0});
        Klientas obj2 = new Klientas("LT123",
                "Jonas", 50.0, new Double[]{50.0, 99.9, 120000.0});
        Klientas obj3 = new Klientas("LT123",
                "Andrius", 50.0, new Double[]{50.0, 99.9, 12000.0});
        Klientas obj4 = new Klientas("LT123",
                "Juozas", 50.0, new Double[]{50.0, 99.9, 100000.0});
        Klientas[] masyvas = new Klientas[]{obj1, obj2, obj3, obj4};

        Klientas[] atrinkti = gautiKlientusSuDideliuVidurkiu(masyvas);
        System.out.println(Arrays.toString(atrinkti));
    }

    public static Klientas[] gautiKlientusSuDideliuVidurkiu(Klientas[] masyvas) {
        Klientas[] atrinkti = new Klientas[0];
        Double mokejimuVidurkis = mokejimuVidurkis(masyvas);
        for (int i = 0; i < masyvas.length; i++) {
            Double[] sumuMasyvas = masyvas[i].getSumuMasyvas();
            for (int j = 0; j < sumuMasyvas.length; j++) {
                if (sumuMasyvas[j] > mokejimuVidurkis * 2) {
                    atrinkti = pridetiElementa(atrinkti, masyvas[i]);
                }
            }
        }
        return atrinkti;
    }

    public static Klientas[] pridetiElementa(Klientas[] masyvas, Klientas elementas) {
        masyvas = Arrays.copyOf(masyvas, masyvas.length + 1);
        masyvas[masyvas.length - 1] = elementas;
        return masyvas;
    }

    public static Double mokejimuVidurkis(Klientas[] masyvas) {
        Double suma = 0d;
        Integer mokejimuKiekis = 0;
        for (int i = 0; i < masyvas.length; i++) {
            Double[] sumuMasyvas = masyvas[i].getSumuMasyvas();
            for (int j = 0; j < sumuMasyvas.length; j++) {
                suma += sumuMasyvas[j];
                mokejimuKiekis++;
            }

        }
        return suma / mokejimuKiekis;
    }
}
