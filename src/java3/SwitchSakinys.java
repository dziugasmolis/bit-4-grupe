package java3;

import java.util.Scanner;

public class SwitchSakinys {
    public static void main(String[] args) {
        Scanner skaitytuvas = new Scanner(System.in);
        System.out.println("Iveskite savaites diena skaiciumi");
        Integer savaitesDiena = skaitytuvas.nextInt();

        switch (savaitesDiena) {
            case 1:
                System.out.println("Pirmadienis");
                break;
            case 2:
                System.out.println("Antradienis");
                break;
            case 3:
                System.out.println("Treciadienis");
                break;
            case 4:
                System.out.println("Ketvirtadienis");
                break;
            case 5:
                System.out.println("Penktadienis");
                break;
            default:
                System.out.println("Tokios dienos nera");

        }
    }
}
