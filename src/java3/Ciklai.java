package java3;

public class Ciklai {
    public static void main(String[] args) {
//        for(int i = 0; i < 101; i++) {
//            System.out.println(i);
//        }
//        for (int i = 100; i >= 0; i--) {
//            System.out.println(i);
//        }
//
//        int i = 0;
//        while (i < 100) {
//            System.out.println(i);
//            i++;
//        }
//
//        i = 100;
//        while (i > 0) {
//            System.out.println(i);
//            i--;
//        }

        for(int j = 0; j < 5; j++) {
            System.out.println("j = " + j);
            for(int z = 0; z < 3; z++) {
                System.out.println("z = " + z);
            }
        }
    }
}
