package java3;

import java.io.*;

public class ZodziuKiekis {

    public static void main(String[] args) {
        String failoKelias = new File("").getAbsolutePath()
                + "/src/java3/Duomenys.txt";

        String rezultatai = new File("").getAbsolutePath()
                + "/src/java3/Rezultatai.txt";
        String duomenuTekstas = "";
        try (BufferedReader skaitytuvas = new BufferedReader(new FileReader(failoKelias))) {
            String eilute = skaitytuvas.readLine();
            while (eilute != null) {
                duomenuTekstas += eilute + " ";
                eilute = skaitytuvas.readLine();
            }

        } catch (FileNotFoundException ex) {
            System.out.println("Failas nerastas");
        } catch (Exception e) {
            System.out.println(e);
        }

        try (BufferedWriter spausdinimas = new BufferedWriter(new FileWriter(rezultatai))) {
            spausdinimas.write("Zodziu kiekis = " + duomenuTekstas.split(" ").length + "\n");
            spausdinimas.write(duomenuTekstas);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
