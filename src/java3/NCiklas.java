package java3;

import java.io.*;

public class NCiklas {

    public static void main(String[] args) {
        String failoKelias = new File("").getAbsolutePath()
                + "/src/java3/Duomenys.txt";

        String rezultatai = new File("").getAbsolutePath()
                + "/src/java3/Rezultatai.txt";
        Integer n = null;
        try (BufferedReader skaitytuvas = new BufferedReader(new FileReader(failoKelias))) {
            String eilute = skaitytuvas.readLine();
             n = Integer.parseInt(eilute);

        } catch (FileNotFoundException ex) {
            System.out.println("Failas nerastas");
        } catch (Exception e) {
            System.out.println(e);
        }

        try (BufferedWriter spausdinimas = new BufferedWriter(new FileWriter(rezultatai))) {
            for(int i = 0; i < n; i++) {
                spausdinimas.write(i + "\n");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
