package java3;

public class ForCiklas {

    public static void main(String[] args) {
        Integer suma = 0;
        for (int i = 0; i < 5; i++) {
            suma += i;
            System.out.println(suma);
        }
        System.out.println("aaaa");

        suma = 0;
        Integer i = 0;
        while (i < 5) {
            suma += i;
            System.out.println(suma);
            i++;
        }
        System.out.println("aaaa");
        suma = 0;
        i = 0;
        do {
            suma += i;
            System.out.println(suma);
            i++;
        }
        while (i < 0);
        System.out.println("aaaa");

        Integer[] masyvas = {1, 2, 3, 4, 5};

        suma = 0;
        for (Integer abcd: masyvas) {

            if(abcd % 2 != 0) {
                continue;
            }
            System.out.println(abcd);
        }

        System.out.println("Suma = " + suma);
        for(Integer skaicius: masyvas) {
            System.out.println(skaicius);
        }


    }
}
