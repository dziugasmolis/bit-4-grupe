package java3;

import java.io.*;

public class Vidurkis {
    public static void main(String[] args) {
        String failoKelias = new File("").getAbsolutePath()
                + "/src/java3/Duomenys.txt";

        String rezultatai = new File("").getAbsolutePath()
                + "/src/java3/Rezultatai.txt";
        Integer kiekis = 0;
        Integer suma = 0;

        Character[] masyvas = {'d', 'c', 'd'};

        try (BufferedReader skaitytuvas = new BufferedReader(new FileReader(failoKelias))) {
            String eilute = skaitytuvas.readLine();
            String[] eilReiksmes = eilute.split(" ");
            kiekis = eilReiksmes.length;
            for (int i = 0; i < eilReiksmes.length; i++) {
                Integer skaicius = Integer.parseInt(eilReiksmes[i]);
                suma += skaicius;
            }

        } catch (FileNotFoundException ex) {
            System.out.println("Failas nerastas");
        } catch (Exception e) {
            System.out.println(e);
        }

        try (BufferedWriter spausdinimas = new BufferedWriter(new FileWriter(rezultatai))) {
            spausdinimas.write("Vidurkis = " + suma / kiekis);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
