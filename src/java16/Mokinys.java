package java16;

import java.time.LocalDate;

public class Mokinys {

    private String vardas;
    private String pavarde;
    private LocalDate gimimoData;

    public Mokinys(String vardas, String pavarde, LocalDate gimimoData) {
        this.vardas = vardas;
        this.pavarde = pavarde;
        this.gimimoData = gimimoData;
    }

    @Override
    public String toString() {
        return "Mokinys{" +
                "vardas='" + vardas + '\'' +
                ", pavarde='" + pavarde + '\'' +
                ", gimimoData=" + gimimoData +
                '}' + "\n";
    }

    public String getVardas() {
        return vardas;
    }

    public void setVardas(String vardas) {
        this.vardas = vardas;
    }

    public String getPavarde() {
        return pavarde;
    }

    public void setPavarde(String pavarde) {
        this.pavarde = pavarde;
    }

    public LocalDate getGimimoData() {
        return gimimoData;
    }

    public void setGimimoData(LocalDate gimimoData) {
        this.gimimoData = gimimoData;
    }
}
