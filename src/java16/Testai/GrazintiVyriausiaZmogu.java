package java16.Testai;

import java.util.List;

public class GrazintiVyriausiaZmogu {

    public static Zmogus grazintiVyriausia(List<Zmogus> zmones) {
        Integer amzius = 0;
        Zmogus vyriausias = null;
        for (Zmogus zmogus: zmones) {
            if(amzius < zmogus.getAmzius()) {
                vyriausias = zmogus;
                amzius = zmogus.getAmzius();
            }
        }
        return vyriausias;
    }
}
