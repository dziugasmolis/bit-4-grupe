package java16.Testai;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class ZmogausTestai {

    @org.junit.jupiter.api.Test
    void testasGautiAmziu() {
        Zmogus zmogus = new Zmogus("Jonas", "Jonaitis", 14);
        Integer amzius = zmogus.getAmzius();
        assertEquals(14, amzius);
    }

    //
    @org.junit.jupiter.api.Test
    void testasGautiVardaPavarde() {
        Zmogus zmogus = new Zmogus("Jonas", "Jonaitis", 14);
        String vardasPavarde = zmogus.getVardasPavarde();
        assertNotEquals("JonasJonaitis", vardasPavarde);
    }

    @org.junit.jupiter.api.Test
    void testasGautiVardaPavardeTeisingai() {
        Zmogus zmogus = new Zmogus("Jonas", "Jonaitis", 14);
        String vardasPavarde = zmogus.getVardasPavarde();
        assertEquals("Jonas Jonaitis", vardasPavarde);
    }

    @org.junit.jupiter.api.Test
    void testasRastiVyriausiaZmogu() {
        List<Zmogus> zmones = new ArrayList<>();
        Zmogus zmogus1 = new Zmogus("Jonas", "Jonaitis", 14);
        Zmogus zmogus2 = new Zmogus("Jonas", "Jonaitis", 19);
        Zmogus zmogus3 = new Zmogus("Jonas", "Jonaitis", 10);
        Zmogus zmogus4 = new Zmogus("Jonas", "Jonaitis", 20);
        zmones.add(zmogus1);
        zmones.add(zmogus2);
        zmones.add(zmogus3);
        zmones.add(zmogus4);

        Zmogus vyriausias = GrazintiVyriausiaZmogu.grazintiVyriausia(zmones);

        assertEquals(20, vyriausias.getAmzius());
    }
}
