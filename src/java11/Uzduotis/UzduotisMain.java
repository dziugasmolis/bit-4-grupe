package java11.Uzduotis;

import java10.Klientas;

import java.util.Arrays;

public class UzduotisMain {

    public static void main(String[] args) {
        Darbuotojas darbuotojas1 = new Darbuotojas("Petras",
                500d, new Darbuotojas.Adresas("Kaunas",
                "Taikos pr.", 15));
        Darbuotojas darbuotojas2 = new Darbuotojas("Juozas",
                700d, new Darbuotojas.Adresas("Kaunas",
                "Savanoriu", 15));
        Darbuotojas darbuotojas3 = new Darbuotojas("Andrius",
                900d, new Darbuotojas.Adresas("Vilnius",
                "Savanoriu", 99));
        Darbuotojas darbuotojas4 = new Darbuotojas("Benas",
                1000d, new Darbuotojas.Adresas("Vilnius",
                "Savanoriu", 19));
        Darbuotojas darbuotojas5 = new Darbuotojas("Vytas",
                400d, new Darbuotojas.Adresas("Klaipeda",
                "Savanoriu", 15));
        Darbuotojas darbuotojas6 = new Darbuotojas("Vytas",
                400d, new Darbuotojas.Adresas("Panevezys",
                "Savanoriu", 15));


        Darbuotojas[] darbuotojai = new Darbuotojas[]{darbuotojas1,
                darbuotojas2, darbuotojas3, darbuotojas4, darbuotojas5, darbuotojas6};
        System.out.println(Arrays.toString(darbuotojai));
        String[] miestai = skirtingiMiestai(darbuotojai);
//        System.out.println(Arrays.toString(miestai));


        Alga pakeliamPusantroKarto = new Alga() {
            @Override
            public Double pakeltiAlga(Double alga) {
                return alga * 1.5;
            }
        };

        Alga pakeliamDuKartus = new Alga() {
            @Override
            public Double pakeltiAlga(Double alga) {
                return alga * 2;
            }
        };

        pakeliamDarbuotojuAlga(darbuotojai, pakeliamPusantroKarto);
//        pakeliamDarbuotojuAlga(darbuotojai, pakeliamDuKartus);
        System.out.println(Arrays.toString(darbuotojai));
    }

    public static void pakeliamDarbuotojuAlga(Darbuotojas[] darbuotojai, Alga pakeliam) {
        for (int i = 0; i < darbuotojai.length; i++) {
            darbuotojai[i].setAtlyginimas(
                    pakeliam.pakeltiAlga(darbuotojai[i].getAtlyginimas()));
        }
    }

    public static String[] skirtingiMiestai(Darbuotojas[] darbuotojai) {
        String[] unikalusMiestai = new String[0];
        for (int i = 0; i < darbuotojai.length; i++) {
            String miestas = darbuotojai[i].getAdresas().getMiestas();
            if (i == 0) {
                unikalusMiestai = pridetiElementa(unikalusMiestai,
                        miestas);
            }
            for (int j = 0; j < unikalusMiestai.length; j++) {
                if (!arEgzistuojaMasyve(unikalusMiestai, miestas)) {
                    unikalusMiestai = pridetiElementa(unikalusMiestai,
                            miestas);
                    break;
                }
            }

        }
        return unikalusMiestai;
    }

    public static Boolean arEgzistuojaMasyve(String[] masyvas, String miestas) {
        for (String elem : masyvas) {
            if (elem.equals(miestas)) {
                return true;
            }
        }
        return false;
    }

    public static String[] pridetiElementa(String[] masyvas,
                                           String elementas) {
        masyvas = Arrays.copyOf(masyvas, masyvas.length + 1);
        masyvas[masyvas.length - 1] = elementas;
        return masyvas;
    }


}
