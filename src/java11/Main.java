package java11;

import java11.vidineklase.A;

public class Main {
    public static void main(String[] args) {
        System.out.println(Konstantos.legalusAlkoAmzius);
        // Kuriame vidine klase kai ji nera static
//        A aKlase = new A();
//        A.B bPaprasta = aKlase.new B();


        // Kuriame vidine klase kai ji yra static
        A.B b = new A.B();

        System.out.println(b.skaicius);
    }
}
