package java11.AnonimineKlase;

public class Main {

    public static void main(String[] args) {
//        Skaiciavimai skaiciavimai = new Skaiciavimai;
        Skaiciavimai obj = new Skaiciavimai() {
            @Override
            public void suma(Integer a, Integer b) {
                System.out.println("Suma = " + (a + b));
            }
            @Override
            public Double plotas(Integer a, Integer b) {
                return 9999d;
            }
        };
        obj.suma(2,2);
//        obj.plotas()
        System.out.println("Plotas = " + obj.plotas(2,2));
    }
}
