package java17;

public class Kriterijus {

    private Integer kambariuSkNuo;
    private Integer kambariuSkIki;

    private Double kvadraturaNuo;
    private Double kvadraturaIki;

    private Double kainaNuo;
    private Double kainaIki;

    public Kriterijus() {}

    public Kriterijus(Integer kambariuSkNuo, Integer kambariuSkIki, Double kvadraturaNuo, Double kvadraturaIki, Double kainaNuo, Double kainaIki) {
        this.kambariuSkNuo = kambariuSkNuo;
        this.kambariuSkIki = kambariuSkIki;
        this.kvadraturaNuo = kvadraturaNuo;
        this.kvadraturaIki = kvadraturaIki;
        this.kainaNuo = kainaNuo;
        this.kainaIki = kainaIki;
    }

    public Integer getKambariuSkNuo() {
        return kambariuSkNuo;
    }

    public void setKambariuSkNuo(Integer kambariuSkNuo) {
        this.kambariuSkNuo = kambariuSkNuo;
    }

    public Integer getKambariuSkIki() {
        return kambariuSkIki;
    }

    public void setKambariuSkIki(Integer kambariuSkIki) {
        this.kambariuSkIki = kambariuSkIki;
    }

    public Double getKvadraturaNuo() {
        return kvadraturaNuo;
    }

    public void setKvadraturaNuo(Double kvadraturaNuo) {
        this.kvadraturaNuo = kvadraturaNuo;
    }

    public Double getKvadraturaIki() {
        return kvadraturaIki;
    }

    public void setKvadraturaIki(Double kvadraturaIki) {
        this.kvadraturaIki = kvadraturaIki;
    }

    public Double getKainaNuo() {
        return kainaNuo;
    }

    public void setKainaNuo(Double kainaNuo) {
        this.kainaNuo = kainaNuo;
    }

    public Double getKainaIki() {
        return kainaIki;
    }

    public void setKainaIki(Double kainaIki) {
        this.kainaIki = kainaIki;
    }
}
