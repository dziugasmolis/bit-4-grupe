package java17;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Mapas {
    public static void main(String[] args) {
        Map<String, Integer> mapas = new HashMap<>();
        mapas.put("raktas", 1);
        mapas.put("raktas2", 2);
        System.out.println(mapas.get("raktas2"));


        for(Integer reiksme: mapas.values()) {
            System.out.println(reiksme);
        }

        for (String raktas: mapas.keySet()) {
            System.out.println(raktas);
        }

        for(Map.Entry<String, Integer> entry : mapas.entrySet()) {
            System.out.println(entry);
        }
        String ieskomasRaktas = "x";
        if (mapas.containsKey(ieskomasRaktas)) {
            System.out.println(ieskomasRaktas + " Rastas");
        }

        if (mapas.containsValue(5)) {
            System.out.println("Rasta reiksme");
        }

        Map<Integer, List<String>> listasMape = new HashMap<>();
        List<String> zodziai = new ArrayList<>();
        zodziai.add("Labas");
        listasMape.put(1, zodziai);
//        List<String> gautasMapoListas = listasMape.get(1);
        zodziai.add("Viso gero");

        System.out.println(listasMape);
    }

    public static void atspausdinti(Map<String, Integer> mapas) {
        System.out.println(mapas);
    }
}
