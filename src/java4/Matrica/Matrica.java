package java4.Matrica;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class Matrica {
    public static void main(String[] args) {
        String duomenuFailoDirektorija = new File("").getAbsolutePath()
                + "/src/Java4/Matrica/Duomenys.txt"; // Duomenu failo direktorija
//        String rezultatuFailoDirektorija = new File("").getAbsolutePath()
//                + "/src/Java3/SkaiciuNuskaitymas/rezultatai.txt"; // Rezultato failo direktorija
        int[][] matrica = null;
        try (BufferedReader failoSkaitytuvas =
                     new BufferedReader(new FileReader(duomenuFailoDirektorija))) { // atidaromas skaitomas failas
            String eilute = failoSkaitytuvas.readLine(); // nuskaitoma pirma eilute
            String[] isskirtaEilute = eilute.split(" ");
            int eilSkaicius = Integer.parseInt(isskirtaEilute[0]); // Eiluciu kiekis nurodytas faile
            int stulpSkaicius = Integer.parseInt(isskirtaEilute[1]); // Stulpeliu kiekis nurodytas faile
            matrica = new int[eilSkaicius][stulpSkaicius]; // Sukuriam masyva su nuskaitytais dydziais
            eilute = failoSkaitytuvas.readLine();
            int eilutesIndeksas = 0; // Eilutes indeksas skirtas zinoti i kuria matricos eilute iterpiam
            while (eilute != null) { // kol eilute netuscia tol skaitys
                System.out.println(eilute);
                isskirtaEilute = eilute.split(" "); // Isskiriam eilutes skaicius
                for (int i = 0; i < isskirtaEilute.length; i++) {
                    matrica[eilutesIndeksas][i] = Integer.parseInt(isskirtaEilute[i]); // Priskiriam paverte skaicius i int tipa priskiriam matricai
                }
                eilutesIndeksas++;
                eilute = failoSkaitytuvas.readLine(); // nuskaitome zemiau esancia eilute
            }
        } catch (FileNotFoundException ex) {
            System.out.println("Failas nerastas");
        } catch (Exception e) {
            System.out.println("something else went wrong");
        }
        int suma = 0;
        int matricosElementuKiekis = 0;
        for (int i = 0; i < matrica.length; i++) {

            for (int j = 0; j < matrica[i].length; j++) {
                suma += matrica[i][j];
                matricosElementuKiekis++;
            }

        }
        Double vidurkis = suma / matricosElementuKiekis * 1.0;
        System.out.println("Suma = " + suma);
        System.out.println(matricosElementuKiekis);

        System.out.println("Vidurkis = " + vidurkis);

        Double eilutesSuma = 0.0;
        for (int i = 0; i < matrica.length; i++) {

            for (int j = 0; j < matrica[i].length; j++) {
                eilutesSuma += matrica[i][j];
            }
            Double eilutesVidurkis = eilutesSuma / matrica[i].length * 1.0;
            System.out.println("Eilutes suma = " + eilutesSuma);
            System.out.println("Eilutes vidurkis = " + eilutesVidurkis);
        }

        int min = 999;
        int max = 0;
        for (int i = 0; i < matrica.length; i++) {

            for (int j = 0; j < matrica[i].length; j++) {
                if (matrica[i][j] > max) {
                    max = matrica[i][j];
                }
                if (matrica[i][j] < min) {
                    min = matrica[i][j];
                }
            }
            System.out.println("Eilutes max = " + max);
            System.out.println("Eilutes min = " + min);
            min = 999;
            max = 0;
        }
    }
}
