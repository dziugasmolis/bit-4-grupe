package java4;

import java.util.Arrays;

public class Masyvai {
    public static void main(String[] args) {
        Integer[] skaiciai = {1, 2, 3, 99, 4, 5};
        for (int i = 0; i < skaiciai.length; i++) {
            System.out.println(skaiciai[i]);
        }
        System.out.println("---------------------------------------");
//        skaiciai = Arrays.copyOf(skaiciai, skaiciai.length - 1);
//        skaiciai[skaiciai.length-1] = 20;
        Integer[] istrintasPirmasNarys = new Integer[skaiciai.length - 1];
        for (int i = 0; i < skaiciai.length; i++) {
            if(i == 0) {
                continue;
            }
            Integer naujoMasyvoVieta = i - 1;
            istrintasPirmasNarys[naujoMasyvoVieta] = skaiciai[i];
        }

        for (int i = 0; i < istrintasPirmasNarys.length; i++) {
            System.out.println(istrintasPirmasNarys[i]);
        }

//        skaiciai[skaiciai.length-1] = 100;
//
//        for (int i = 0; i < skaiciai.length; i++) {
//            if (skaiciai[i] == 99) {
//                System.out.println("Skaicius 99 indeksas = " + i);
//                break;
//            }
//
//        }
    }
}
