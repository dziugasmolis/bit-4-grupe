package java8;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Mokinys obj = new Mokinys("Petras", "Petraitis",
                14, 8, new Integer[]{4, 5, 6, 7});
        System.out.println(obj);
        Object[] masyvas = {"aaa", 123, 2.0, 'c'};
        System.out.println(Arrays.toString(masyvas));

        obj.Uzklojom();

        Zmogus zmogus = new Mokinys("Petras", "Petraitis",
                14, 8, new Integer[]{4, 5, 6, 7});
        Zmogus[] masyvas2 = {obj};
        if(zmogus instanceof Mokinys) {
            System.out.println("Mokinys");
        }
    }
}
