package java8;

public class Zmogus {
    private String vardas;
    private String pavarde;
    private Integer amzius;

    public Zmogus() {}

    public Zmogus(String vardas, String pavarde, Integer amzius) {
        this.vardas = vardas;
        this.pavarde = pavarde;
        this.amzius = amzius;
    }

    public String toString() {
        return "Vardas: " + vardas + " Pavarde: " + pavarde;
    }

    public void Uzklojom() {
        System.out.println("Tevine");
    }

    public String getVardas() {
        return vardas;
    }

    public void setVardas(String vardas) {
        this.vardas = vardas;
    }

    public String getPavarde() {
        return pavarde;
    }

    public void setPavarde(String pavarde) {
        this.pavarde = pavarde;
    }

    public Integer getAmzius() {
        return amzius;
    }

    public void setAmzius(Integer amzius) {
        this.amzius = amzius;
    }
}
