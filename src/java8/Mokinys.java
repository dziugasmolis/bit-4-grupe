package java8;

import java.util.Arrays;

public class Mokinys extends Zmogus {
    private Integer klase;
    private Integer[] pazymiai;

    public Mokinys(String vardas, String pavarde, Integer amzius,
                   Integer klase, Integer[] pazymiai) {
        super(vardas, pavarde, amzius);
        this.klase = klase;
        this.pazymiai = pazymiai;
    }

    public String toString() {
        return "Vardas: " + getVardas()+ " Pavarde: " + getPavarde()
                + " Klase: " + klase +
                " Pazymiai: " + Arrays.toString(pazymiai);
    }

    @Override
    public void Uzklojom() {
        System.out.println("Vaikine");
    }

    public Integer getKlase() {
        return klase;
    }

    public void setKlase(Integer klase) {
        this.klase = klase;
    }

    public Integer[] getPazymiai() {
        return pazymiai;
    }

    public void setPazymiai(Integer[] pazymiai) {
        this.pazymiai = pazymiai;
    }
}
