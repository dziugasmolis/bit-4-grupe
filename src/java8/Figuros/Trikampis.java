package java8.Figuros;

public class Trikampis extends Figura {
    private Double krastine1;
    private Double krastine2;
    private Double krastine3;

    public Trikampis(String vardas, Double krastine1, Double krastine2, Double krastine3) {
        super(vardas);
        this.krastine1 = krastine1;
        this.krastine2 = krastine2;
        this.krastine3 = krastine3;
    }

    public String toString() {
        return "Figuros pavadinimas: " + getVardas() +
                " Plotas: " + gautiPlota()
                + " Perimetras: " + gautiPerimetra();
    }

    public Double gautiPerimetra() {
        return krastine1 + krastine2 + krastine3;
    }

    public Double gautiPlota() {
        return krastine1 * krastine2 / 2;
    }

}
