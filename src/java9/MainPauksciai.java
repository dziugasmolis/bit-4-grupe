package java9;

public class MainPauksciai {
    public static void main(String[] args) {
        Varna varna = new Varna();
        Kregzde kregzde = new Kregzde();

        Paukstis[] pauksciai = {varna, kregzde};

        System.out.println(pauksciai[0].gautiPavadinima());
    }
}
