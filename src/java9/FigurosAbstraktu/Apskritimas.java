package java9.FigurosAbstraktu;

public class Apskritimas extends Figura{

    private Double r;

    public Apskritimas(String pavadinimas, Double r) {
        super(pavadinimas);
        this.r = r;
    }

    public String toString() {
        return "Figuros pavadinimas: " + getPavadinimas() +
                " Plotas: " + gautiPlota()
                + " Perimetras: " + gautiPerimetra();
    }

    @Override
    public Double gautiPerimetra() {
        return 2* Math.PI * r;
    }

    @Override
    public Double gautiPlota() {
        return Math.PI * (r*r);
    }
}
