package java9.FigurosAbstraktu;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        Apskritimas apskritimas = new Apskritimas("aps", 3.0);
        Kvadratas kvadratas = new Kvadratas("kvad", 2.0);
        LygiakrastisTrikampis lygiakrastisTrikampis = new LygiakrastisTrikampis("trik", 6.0);

        Figura[] figuros = {apskritimas, kvadratas, lygiakrastisTrikampis};
        System.out.println(Arrays.toString(figuros));
    }
}
