package java9.FigurosAbstraktu;

public abstract class Figura {
    private String pavadinimas;

    public Figura(String pavadinimas) {
        this.pavadinimas = pavadinimas;
    }

    public abstract Double gautiPerimetra();

    public abstract Double gautiPlota();

    public String getPavadinimas() {
        return pavadinimas;
    }

    public void setPavadinimas(String pavadinimas) {
        this.pavadinimas = pavadinimas;
    }
}
