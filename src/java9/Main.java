package java9;

import java9.FigurosAbstraktu.Figura;
import java9.FigurosAbstraktu.Kvadratas;

public class Main {
    public static void main(String[] args) {
        Kvadratas kvadratas = new Kvadratas("kvad", 2.0);
        Figura[] figuros = {kvadratas};

        System.out.println(figuros[0].gautiPlota());
    }
}
