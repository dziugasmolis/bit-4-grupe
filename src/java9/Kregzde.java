package java9;

public class Kregzde extends Paukstis {

    public String gautiPavadinima() {
        return "Kregzde";
    }

    public Integer gautiAmziu() {
        return 2;
    }

    public Double gautiSvori() {
        return 1.0;
    }
}
