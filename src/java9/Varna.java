package java9;

public class Varna extends Paukstis {

    public String gautiPavadinima() {
        return "Varna";
    }

    public Integer gautiAmziu() {
        return 5;
    }

    public Double gautiSvori() {
        return 3.0;
    }
}
