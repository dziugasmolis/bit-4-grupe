package java9;

public abstract class Paukstis {

    public abstract String gautiPavadinima();
    public abstract Integer gautiAmziu();
    public abstract Double gautiSvori();
}
