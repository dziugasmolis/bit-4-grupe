package java2;

import java.util.Scanner;

public class NustatytiTipa {
    public static void main(String[] args) {
        Scanner skaitytuvas = new Scanner(System.in);
        System.out.println("Iveskite reiksme");
        Object kintamasis = skaitytuvas.next();

        if(kintamasis instanceof String) {
            System.out.println("Kintamasis String tipo");
        } else if (kintamasis instanceof Integer) {
            System.out.println("Kintamasis Integer tipo");
        }
    }
}
