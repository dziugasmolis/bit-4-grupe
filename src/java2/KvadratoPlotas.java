package java2;

import java.util.Scanner;

public class KvadratoPlotas {

    public static void main(String[] args) {
        Scanner skaitytuvas = new Scanner(System.in);
        System.out.println("Iveskite kvadrato krastine");
        Double krastine = skaitytuvas.nextDouble();
        Double plotas = Math.pow(krastine, 2);
        plotas = krastine * krastine;
    }
}
