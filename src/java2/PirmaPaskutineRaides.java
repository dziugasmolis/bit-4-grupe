package java2;

import java.util.Scanner;

public class PirmaPaskutineRaides {
    public static void main(String[] args) {
        Scanner skaitytuvas = new Scanner(System.in);
        System.out.println("Iveskite zodi");
        String zodis = skaitytuvas.next();
        System.out.println("Pirma zodzio raide = " + zodis.charAt(0));
    }
}
