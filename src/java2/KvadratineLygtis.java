package java2;

import java.util.Scanner;

public class KvadratineLygtis {
    public static void main(String[] args) {
        Scanner skaitytuvas = new Scanner(System.in);
        System.out.println("Iveskite skaiciu a");
        Double a = skaitytuvas.nextDouble();
        System.out.println("Iveskite skaiciu b");
        Double b = skaitytuvas.nextDouble();
        System.out.println("Iveskite skaiciu c");
        Double c = skaitytuvas.nextDouble();
        skaitytuvas.close();

        Double d = Math.pow(b, 2) - 4 * a * c;
        System.out.println("Diskriminantas = " + d);
        if (d > 0) {
            Double x1 = (-b + Math.sqrt(d)) / 2 * a;
            Double x2 = (-b - Math.sqrt(d)) / 2 * a;
            System.out.println("x1 = " + x1);
            System.out.println("x2 = " + x2);
        } else if (d == 0) {
            Double x = - b / 2 * a;
            System.out.println("x = " + x);
        }
    }
}
