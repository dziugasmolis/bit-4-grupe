package java2;

import java.util.Scanner;

public class DidziausiasSkaicius {
    public static void main(String[] args) {
        Scanner skaitytuvas = new Scanner(System.in);
        System.out.println("Iveskite pirma skaiciu");
        Double pirmasSkaicius = skaitytuvas.nextDouble();

        System.out.println("Iveskite antra skaiciu");
        Double antrasSkaicius = skaitytuvas.nextDouble();

        System.out.println("Iveskite trecia skaiciu");
        Double treciasSkaicius = skaitytuvas.nextDouble();

        skaitytuvas.close();

        if (pirmasSkaicius > antrasSkaicius && pirmasSkaicius > treciasSkaicius) {
            System.out.println("Pirmas skaicius didziausias");
        } else if (antrasSkaicius > pirmasSkaicius && antrasSkaicius > treciasSkaicius) {
            System.out.println("Antras skaicius didziausias");
        } else {
            System.out.println("Trecias skaicius didziausias");
        }
    }
}
