package java13.demo2;
@FunctionalInterface
public interface Avg<T> {

    T avg(T n1, T n2);

}
