package java14;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MasinuMain {

    public static void main(String[] args) {
        String failoKelias = new File("").getAbsolutePath()
                + "/src/java14/Duomenys.txt";
        List<Car> masinos = new ArrayList<>();
        skaitymas(failoKelias, masinos);
        Collections.sort(masinos);
        System.out.println(masinos);
        System.out.println("Masinos tarp 2000 ir 2010 metu");
        List<Car> atrintos = rastiMasinasTarpMetu(masinos, 2000, 2010);
        System.out.println(atrintos);
    }

    public static List<Car> rastiMasinasTarpMetu(List<Car> masinos,
                                                 Integer nuo,
                                                 Integer iki) {
        List<Car> atrinktos = new ArrayList<>();
        for(int i = 0; i < masinos.size(); i++) {
            if (masinos.get(i).getMetai() > nuo &&
                    masinos.get(i).getMetai() < iki) {
                atrinktos.add(masinos.get(i));
            }
        }
        return atrinktos;
    }

    public static void skaitymas(String failas,List<Car> masinos) {
        try (BufferedReader skaitytuvas = new BufferedReader(new FileReader(failas))) {
            String eilute = skaitytuvas.readLine();
            Integer kiekEiluciu = Integer.parseInt(eilute);
            eilute = skaitytuvas.readLine();
            for (int i = 0; i < kiekEiluciu; i++) {
                String[] eilutesDuomenys = eilute.split(" ");
                String marke = eilutesDuomenys[0];
                String modelis = eilutesDuomenys[1];
                Integer metai = Integer.parseInt(eilutesDuomenys[2]);
                Double kaina = Double.parseDouble(eilutesDuomenys[3]);
                Double turis = Double.parseDouble(eilutesDuomenys[4]);
                String kuroTipas = eilutesDuomenys[5];
                Car masina = new Car(marke, modelis, metai, kaina, turis, kuroTipas);
                masinos.add(masina);
                eilute = skaitytuvas.readLine();
            }
        } catch (FileNotFoundException ex) {
            System.out.println("Failas nerastas");
        } catch (Exception e) {
            System.out.println(e);
        }
    }


}
