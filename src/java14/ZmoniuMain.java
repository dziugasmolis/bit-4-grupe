package java14;

import java.util.HashMap;
import java.util.Map;

public class ZmoniuMain {

    public static void main(String[] args) {
        Zmogus obj1 = new Zmogus("petras", "petraitis", "123");
        Map<String, Zmogus> mapas = new HashMap<>();
        mapas.put(obj1.getAsmensKodas(), obj1);
        System.out.println(mapas);
    }
}

