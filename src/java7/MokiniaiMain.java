package java7;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class MokiniaiMain {
    public static void main(String[] args) {
        String failoKelias = new File("").getAbsolutePath()
                + "/src/java7/Studentai.txt";
        Mokinys[] mokiniai2 = new Mokinys[0];
//        java8.Mokinys obj = new java8.Mokinys();
        Mokinys[] mokiniai = skaityti(failoKelias);
        spausdinti(mokiniai);
        Mokinys geriausias = rastiGeriausia(mokiniai);
        System.out.println("Geriausias mokinys: ");
        System.out.println(geriausias);
    }

    public static void spausdinti(Mokinys[] mokiniai) {
        for(int i = 0; i < mokiniai.length; i++) {
            System.out.println(mokiniai[i]);
        }
    }

    public static Mokinys rastiGeriausia(Mokinys[] mokiniai) {
        Double max = 0d;
        Integer indeksas = 0;
        for (int i = 0; i < mokiniai.length; i++) {
            if(max < mokiniai[i].vidurkis()) {
                max = mokiniai[i].vidurkis();
                indeksas = i;
            }
        }
        return mokiniai[indeksas];
    }

    public static Mokinys[] skaityti(String failoKelias) {
        Mokinys[] mokiniai = new Mokinys[5];
        try (BufferedReader skaitytuvas = new BufferedReader(new FileReader(failoKelias))) {
            String eilute = skaitytuvas.readLine();
            Integer indeksas = 0;
            while (eilute != null) {
                String[] eilDuomenys = eilute.split(" ");
                String vardas = eilDuomenys[0];
                String pavarde = eilDuomenys[1];
                Integer klase = Integer.parseInt(eilDuomenys[2]);
                Integer[] pazymiai = new Integer[4];
                pazymiai[0] = Integer.parseInt(eilDuomenys[3]);
                pazymiai[1] = Integer.parseInt(eilDuomenys[4]);
                pazymiai[2] = Integer.parseInt(eilDuomenys[5]);
                pazymiai[3] = Integer.parseInt(eilDuomenys[6]);
                Mokinys objektas = new Mokinys(vardas, pavarde, klase, pazymiai);

                mokiniai[indeksas] = objektas;
                indeksas++;

                eilute = skaitytuvas.readLine();
            }

        } catch (FileNotFoundException ex) {
            System.out.println("Failas nerastas");
        } catch (Exception e) {
            System.out.println(e);
        }
        return mokiniai;
    }
}
