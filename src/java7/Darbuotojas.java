package java7;

class Darbuotojas {
    private String vardas;
    private String pavarde;
    private Integer amzius;
    private String pareigybes;

    public Darbuotojas(String vardas, String pavarde,
                       Integer amzius, String pareigybes) {
        this.vardas = vardas;
        this.pavarde = pavarde;
        this.amzius = amzius;
        this.pareigybes = pareigybes;
    }

    public Darbuotojas(String vardas, String pavarde, Integer amzius) {
        this.vardas = vardas;
        this.pavarde = pavarde;
        this.amzius = amzius;
        this.pareigybes = "bedarbis";
    }

    public String toString() {
        return "Vardas: " + vardas + " Pavarde: " + pavarde +
                " Amzius: " + amzius + " Pareigybes: " + pareigybes;
    }

    public String getVardas() {
        return vardas;
    }

    public void setVardas(String vardas) {
        this.vardas = vardas;
    }

    public String getPavarde() {
        return pavarde;
    }

    public void setPavarde(String pavarde) {
        this.pavarde = pavarde;
    }

    public Integer getAmzius() {
        return amzius;
    }

    public void setAmzius(Integer amzius) {
        this.amzius = amzius;
    }

    public String getPareigybes() {
        return pareigybes;
    }

    public void setPareigybes(String pareigybes) {
        this.pareigybes = pareigybes;
    }
}
