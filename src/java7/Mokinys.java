package java7;

import java.util.Arrays;

public class Mokinys {
    private String vardas;
    private String pavarde;
    private Integer klase;
    private Integer[] pazymiai;


    public Mokinys(String vardas, String pavarde, Integer klase, Integer[] pazymiai) {
        this.vardas = vardas;
        this.pavarde = pavarde;
        this.klase = klase;
        this.pazymiai = pazymiai;
    }

    public Double vidurkis() {
        Double suma = 0d;
        for(int i = 0; i < pazymiai.length; i++){
            suma += pazymiai[i];
        }
        return suma / pazymiai.length;
    }

    @Override
    public String toString() {
        return "Mokinys{" +
                "vardas='" + vardas + '\'' +
                ", pavarde='" + pavarde + '\'' +
                ", klase=" + klase +
                ", pazymiai=" + Arrays.toString(pazymiai) +
                '}';
    }

    public String getVardas() {
        return vardas;
    }

    public void setVardas(String vardas) {
        this.vardas = vardas;
    }

    public String getPavarde() {
        return pavarde;
    }

    public void setPavarde(String pavarde) {
        this.pavarde = pavarde;
    }

    public Integer getKlase() {
        return klase;
    }

    public void setKlase(Integer klase) {
        this.klase = klase;
    }

    public Integer[] getPazymiai() {
        return pazymiai;
    }

    public void setPazymiai(Integer[] pazymiai) {
        this.pazymiai = pazymiai;
    }
}
