package Java19.Prekes;

import Java18.Buitine_Technika.Preke;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        List<Prekes> prekes = new ArrayList<>();
        Scanner skaitytuvas = new Scanner(System.in);
        System.out.println("Pasirinkite koki tipa norite prideti");
        System.out.println("1 - skalbykle, 2 - televizorius");
        Integer pasirinkimas = skaitytuvas.nextInt();
        String tipas;
        System.out.println("Iveskite pavadinima");
        String pavadinimas = skaitytuvas.next();
        System.out.println("Iveskite kieki");
        Integer kiekis = skaitytuvas.nextInt();
        System.out.println("Iveskite vnt. kaina");
        Double vntKaina = skaitytuvas.nextDouble();
        if (pasirinkimas == 1) {
            tipas = "skalbykle";
            System.out.println("Iveskite skalbykles talpa");
            String talpa = skaitytuvas.next();
            Skalbykle naujaSkalbykle = new Skalbykle(1, tipas, pavadinimas, kiekis, vntKaina, talpa);
            prekes.add(naujaSkalbykle);
            System.out.println(naujaSkalbykle.toString());
            // panaudoti ta pati rasymo metoda
        } else if (pasirinkimas == 2) {

        }
    }
}
