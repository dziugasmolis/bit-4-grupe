package Java19.Prekes;

public class Prekes {
    private Integer id;
    private String tipas;
    private String pavadinimas;
    private Integer kiekis;
    private Double vntKaina;

    public Prekes(Integer id, String tipas, String pavadinimas, Integer kiekis, Double vntKaina) {
        this.id = id;
        this.tipas = tipas;
        this.pavadinimas = pavadinimas;
        this.kiekis = kiekis;
        this.vntKaina = vntKaina;
    }

    @Override
    public String toString() {
        return getId() + " " + getTipas() + " " + getPavadinimas() + " " + getKiekis() + " " + getVntKaina();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTipas() {
        return tipas;
    }

    public void setTipas(String tipas) {
        this.tipas = tipas;
    }

    public String getPavadinimas() {
        return pavadinimas;
    }

    public void setPavadinimas(String pavadinimas) {
        this.pavadinimas = pavadinimas;
    }

    public Integer getKiekis() {
        return kiekis;
    }

    public void setKiekis(Integer kiekis) {
        this.kiekis = kiekis;
    }

    public Double getVntKaina() {
        return vntKaina;
    }

    public void setVntKaina(Double vntKaina) {
        this.vntKaina = vntKaina;
    }
}
