package Java19.Prekes;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class EditPrekeMain {
    public static void main(String[] args) {
        List<Prekes> prekes = new ArrayList<>();
        Skalbykle skalbykle = new Skalbykle(1, "skalbykle", "LG", 5, 99.0, "5KG");
        prekes.add(skalbykle);
        Scanner skaitytuvas = new Scanner(System.in);
        System.out.println(prekes);
        System.out.println("Pasirinkite preke kuria norite redaguoti");
        Integer pasirinkimas = skaitytuvas.nextInt();
        Integer prekePagalIDIndeksas = gautiPrekePagalID(prekes, pasirinkimas);
        if (prekePagalIDIndeksas != null) {
            System.out.println("Iveskite nauja pavadinima");

            String pavadinimas = skaitytuvas.next();
            prekes.get(prekePagalIDIndeksas).setPavadinimas(pavadinimas);
            Integer kiekis = skaiciuScanneris("Iveskite nauja kieki", skaitytuvas);
            prekes.get(prekePagalIDIndeksas).setKiekis(kiekis);

            System.out.println(prekes);
        } else {
            System.out.println("Tokios prekes nera");
        }


    }


    public static Integer gautiPrekePagalID(List<Prekes> prekes,
                                            Integer id) {
        for (int i = 0; i < prekes.size(); i++) {
            if (prekes.get(i).getId().equals(id)) {
                return i;
            }
        }
        return null;
    }

    public static Integer skaiciuScanneris(String zinute, Scanner skaitytuvas) {
        System.out.println(zinute);
        Integer skaicius = null;
        try {
            skaicius = skaitytuvas.nextInt();
        } catch (InputMismatchException ex) {
            System.out.println("Ivestas blogas formatas");
            skaitytuvas.next();
            skaicius = skaiciuScanneris(zinute, skaitytuvas);
        }
        return skaicius;
    }
}
