package java5;


import java.util.Arrays;

public class Metodai {
    public static void main(String[] args) {
//        Integer[] skaiciai = {1, 2, 3};

//        spausdintiMasyva(skaiciai, "Spausdinu pirma masyva");
//        Integer[] skaiciai2 = {9, 9, 9, 9, 9};
//        keistiMasyva(skaiciai2);
//        spausdintiMasyva(skaiciai2, "Spausdinu antra masyva");
//        Integer suma = dviejuSkaiciuSuma(2,2);
//        System.out.println(suma);
//        spausdinti("Pavadinimas",1,2,3,4,9,12,999);
//        int a = 10;
//        kitas(a);
//        System.out.println(a);

        String a = "Labas";
//        String b = a;
//        changeString(a);
//        System.out.println("a = " + a);
//        System.out.println("b = " + b);
        Integer[] array = {1, 2, 3, 99, 6,8, 74, 11, 4, 5, 15};
        Integer[] naujasMasyas = new Integer[0];
        try {
            naujasMasyas = visiSkaiciaiMazesniUz10(array);
        } catch (Exception ex) {
            System.out.println("Masyvo indeksas neegzistuoja");
        } finally {
            System.out.println("Ivyko galiausiai");
        }
        spausdinti(naujasMasyas);
//        String[] zodziai = {"asd", "a", "test", "labas"};
//        String ilgiausiasZodis = ilgiausiasZodis(zodziai);
//        System.out.println(ilgiausiasZodis);
//        Character vidurineRaide = vidurinisSimbolis(ilgiausiasZodis);
//        System.out.println(vidurineRaide);
//        forLoop(0, array);
//        spausdinti(array);
//        spausdinti(zodziai);
//
//        Integer suma = suma(9, 9);
//        System.out.println(suma);
//        arLyginis(3);
//        Double vidurkis = vidurkis(array);
//        System.out.println("Vidurkis = " + vidurkis);
    }

    public static Double vidurkis(Integer[] masyvas) {
        Double suma = 0d;
        for (int i = 0; i < masyvas.length; i++) {
            suma += masyvas[i];
        }
        return suma / masyvas.length;
    }

    public static void arLyginis(Integer skaicius) {
        if (skaicius % 2 == 0) {
            System.out.println(skaicius + " Skaicius yra lyginis");
        } else {
            System.out.println(skaicius + " Skaicius nera lyginis");
        }
    }

    public static Integer suma(Integer a, Integer b) {
        return a + b;
    }

    public static void spausdinti(Integer[] masyvas) {
        for (int i = 0; i < masyvas.length; i++) {
//            arLyginis(masyvas[i]);
            System.out.println(masyvas[i]);
        }
    }

    public static void spausdinti(String[] masyvas) {
        for (int i = 0; i < masyvas.length; i++) {
            System.out.println(masyvas[i]);
        }
    }

    private static void forLoop(int i, int[] array) {
        if (i < array.length) {
            System.out.println(array[i]);
            forLoop(++i, array);
        }
    }


    private static void changeString(String a) {
        a = a + " vakaras";
    }


    public static void keistiMasyva(Integer[] masyvas) {
        masyvas[1] = 999999;
    }

    public static int kitas(int a) {
        a = 200;
        System.out.println(a);
        return a;
    }


    public static void spausdintiMasyva(Integer[] betkas,
                                        String pavadinimas) {
        System.out.println(pavadinimas);
        if (betkas.length == 3) {
            return;
        }
        for (Integer skaicius : betkas) {
            System.out.println(skaicius);
        }
    }

    public static Integer dviejuSkaiciuSuma(Integer a, Integer b) {
        return a + b;
    }

    public static void spausdinti(String pavadinimas, Integer... skaiciai) {
        System.out.println(pavadinimas);
        for (Integer skaicius : skaiciai) {
            System.out.println(skaicius);
        }
    }

    public static Integer[] visiSkaiciaiMazesniUz10(Integer[] skaiciai)
            throws ArrayIndexOutOfBoundsException {
        Integer[] naujasMasyvas = new Integer[0];
        for (int i = 0; i < skaiciai.length; i++) {
            if (skaiciai[i] < 10) {
                naujasMasyvas = pridetiElementa(naujasMasyvas, skaiciai[i]);
            }
        }
        return naujasMasyvas;
    }

    public static Integer[] pridetiElementa(Integer[] masyvas, Integer elementas) {
        masyvas = Arrays.copyOf(masyvas, masyvas.length + 1);
        masyvas[masyvas.length - 1] = elementas;
        return masyvas;
    }

    public static String ilgiausiasZodis(String[] zodziai) {
        String ilgiausiasZodis = "";
        for (int i = 0; i < zodziai.length; i++) {
            if (zodziai[i].length() > ilgiausiasZodis.length()) {
                ilgiausiasZodis = zodziai[i];
            }
        }
        return ilgiausiasZodis;
    }

    public static Character vidurinisSimbolis(String zodis) {
        Integer vidurinesRaidesIndeksas = zodis.length() / 2;
        return zodis.charAt(vidurinesRaidesIndeksas);
    }
}
