package java1;

import java.util.Scanner;

public class StaciakampoPlotas {
    public static void main(String[] args) {
        Scanner skaitytuvas = new Scanner(System.in);
        System.out.println("Iveskite krastine a");
        Double a = skaitytuvas.nextDouble();
        System.out.println("Iveskite krastine b");
        Double b = skaitytuvas.nextDouble();
        skaitytuvas.close();
        Double plotas = a * b;
        String result = String.format("%.2f", plotas);
        System.out.println("Plotas = " + result);
    }
}
