package Java18.demo5_Factory_method;

public class Lenkas implements IPerson {

    @Override
    public String getName() {
        return "Lenkas";
    }

    @Override
    public String toString() {
        return "Lenkas {" + getName() + "}";
    }
}
